import {Injectable} from "@angular/core";
import {ToastController} from 'ionic-angular';

@Injectable()
export class SiteGlobalFunctionsService {
    public tokenActive: boolean = false;
    public loading;

    constructor(
        public toastCtrl: ToastController) {
    }

    // ____________________________________________________
    public onClickCancelProcessFunction(element) {
        element.onclick = function() {
            // console.log('Cancel this process.');

            window.location.reload(true);
        };
    }

    public onActivatePleaseWaitBlock() {
        const cancelProcessButton = document.querySelector('#cancel-process-button');
        const mcAppBody = document.querySelector('#mc-app-body');
        const globalMessageWrap = document.querySelector('#global-message-wrap');
        const globalWaitingBlock = document.querySelector('#global-waiting-block');
        const globalMessageTingBlock = document.querySelector('#global-message-tint-block');

        this.onClickCancelProcessFunction(cancelProcessButton);

        mcAppBody.classList.add('kill-body-scroll');
        globalMessageWrap.classList.add('display-element');
        globalWaitingBlock.classList.add('display-element');
        globalMessageTingBlock.classList.add('display-element');
    }

    public onDeactivatePleaseWaitBlock() {
        const mcAppBody = document.querySelector('#mc-app-body');
        const globalMessageWrap = document.querySelector('#global-message-wrap');
        const globalWaitingBlock = document.querySelector('#global-waiting-block');
        const globalMessageTingBlock = document.querySelector('#global-message-tint-block');

        mcAppBody.classList.remove('kill-body-scroll');
        globalMessageWrap.classList.remove('display-element');
        globalWaitingBlock.classList.remove('display-element');
        globalMessageTingBlock.classList.remove('display-element');
    }

    // ____________________________________________________
    public presentToastTemporary(arg) {
        let toast = this.toastCtrl.create({
            message: arg.message,
            duration: arg.duration,
            position: 'middle'
        });
        toast.present();
    }

    public presentToastClose(arg) {
        let toast = this.toastCtrl.create({
            message: arg.message,
            showCloseButton: true,
            closeButtonText: arg.closeButtonText,
            position: 'middle'
        });
        toast.present();
    }

    // ____________________________________________________
    public validateAnyValue(valueToCheck) {
        return (valueToCheck !== null) &&
            (valueToCheck !== 'null') &&
            (valueToCheck !== 'NaN') &&
            (valueToCheck !== '') &&
            (valueToCheck !== undefined) &&
            (valueToCheck !== false);
    }

    // ____________________________________________________
    public logUserOut(callback) {
        this.removeUserLocalStorageValues();
        callback();
    }

    // ____________________________________________________
    // Remove user values to localStorage
    public removeUserLocalStorageValues() {
        localStorage.removeItem('access_level');
        localStorage.removeItem('account_type');
        localStorage.removeItem('email');
        localStorage.removeItem('firstname');
        localStorage.removeItem('lastname');
        localStorage.removeItem('token');
        localStorage.removeItem('user_code');
        localStorage.removeItem('user_id');
        localStorage.removeItem('username');
        localStorage.removeItem('city');
        localStorage.removeItem('state');
        localStorage.removeItem('zip');
        localStorage.removeItem('profile_image_id');
        localStorage.removeItem('profile_image_version');
    }

    // ____________________________________________________
    // Check to see if number is an integer
    public isInteger(x) {
        return typeof x === "number" && isFinite(x) && Math.floor(x) === x;
    }

    // ____________________________________________________
    // Check to see if number is a floating point.
    public isFloat(x) {
        return !!(x % 1);
    }

    // ____________________________________________________
    // Check to see if a number is a float and if so then sanitize it
    // If not then
    public sanitizeFloatNumber(rawValue) {
        let numberValue = Number(rawValue);

        if (this.isFloat(numberValue)) {
            let string_1Num = String(numberValue) + '0';
            let num_1_change = parseFloat(string_1Num);
            let finalFloatingPointVal = Number((num_1_change).toFixed(2));
            return finalFloatingPointVal;
        } else if (this.isInteger(numberValue)) {
            return numberValue;
        }
    }

    // ____________________________________________________
    // Delay events functions
    public delayActions() {
        let timer = 0;

        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }

    // ____________________________________________________
    // ____________________________________________________

}

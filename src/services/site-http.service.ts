import {Injectable} from "@angular/core";

@Injectable()
export class SiteHttpService {
    // public envType = 'localDev';
    public envType = 'productionDev';
    public apiEnvPrefix: string;
    public ENV_URL: string;

    // HTTP Request Method
    public callHTTPMethodGeoLocation(dataObj, callback) {
        let xhr = new XMLHttpRequest();

        function myRequestCallback() {
            if (xhr.readyState < 4) {
                return;
            }
            if (xhr.status !== 200) {
                return;
            }

            let myJsonRes = JSON.parse(xhr.responseText);

            // Return the json response object
            callback(myJsonRes);
        }

        xhr.onreadystatechange = myRequestCallback;
        xhr.open('GET', dataObj.url, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.send();
    }

    // HTTP Request Method
    public callHTTPMethod(dataObj, callback) {
        let xhr = new XMLHttpRequest();

        if (this.envType === 'localDev') {
            // Local development API Prefix.
            this.apiEnvPrefix = 'http://localhost:3500/api/';
        } else if (this.envType === 'productionDev') {
            // Production development API Prefix.
            this.apiEnvPrefix = 'https://somedomainname.com/api/';
        } else {
            // Just use production prefix
            this.apiEnvPrefix = 'https://somedomainname.com/api/';
        }

        // Check to see if this http call will require a parameter to be passed.
        if (dataObj.param) {
            this.ENV_URL = this.apiEnvPrefix + dataObj.url + '/' + dataObj.param;
        } else {
            this.ENV_URL = this.apiEnvPrefix + dataObj.url;
        }
        xhr.onreadystatechange = myRequestCallback;

        function myRequestCallback() {
            if (xhr.readyState < 4) {
                return;
            }
            if (xhr.status !== 200) {
                return;
            }

            let myJsonRes = JSON.parse(xhr.responseText);

            // Return the json response object
            callback(myJsonRes);
        }

        // Check to see if this is a geolocation call
        if (dataObj.get_geolocation === true) {
            this.ENV_URL = dataObj.url;
        }

        xhr.open(dataObj.httpCallType, this.ENV_URL, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        // Set token request header if needed
        if (dataObj.token) {
            xhr.setRequestHeader('x-access-token', dataObj.token);
        }
        if ((dataObj.httpCallType === 'POST') ||
            (dataObj.httpCallType === 'PUT') ||
            (dataObj.httpCallType === 'DELETE')) {
            // console.log('Do a POST, PUT, or DELETE request.');
            xhr.send(JSON.stringify(dataObj));
        } else {
            // console.log('Just do a regular old GET request.');
            xhr.send();
        }
    }

}

import { Injectable } from '@angular/core';

@Injectable()
export class McValidationsService {

  constructor() { }

    // ____________________________________________________

    public validateAnyValue(valueToCheck) {
        return (valueToCheck !== null) &&
            (valueToCheck !== 'null') &&
            (valueToCheck !== 'NaN') &&
            (valueToCheck !== 0) &&
            (valueToCheck !== '') &&
            (valueToCheck !== undefined) &&
            (valueToCheck !== false);
    }

    // ____________________________________________________

    // Check That Field is not empty function
    public checkFieldNotEmpty(fieldVal) {
        return (fieldVal !== '') &&
            (fieldVal !== undefined) &&
            (fieldVal !== null);
    }

    // ____________________________________________________

    // Check That Field is not empty function minimum of 3 charaters
    public checkFieldNotEmptyAndMin3Characters(fieldVal) {
        return (fieldVal !== '') &&
            (fieldVal !== undefined) &&
            (fieldVal.length > 2);
    }

    // ____________________________________________________

    // Check That Field is not empty function minimum of 8 charaters
    public checkFieldNotEmptyAndMin8Characters(fieldVal) {
        return (fieldVal !== '') &&
            (fieldVal !== undefined) &&
            (fieldVal.length > 7);
    }

    // ____________________________________________________

    // Check that an email address is valid
    public emailRegex(fieldVal) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(fieldVal);
    }

    public checkValidEmail(fieldVal) {
        return this.emailRegex(fieldVal) != false;
    }

    // ____________________________________________________
    // Check to see if number is an integer
    public isInteger(x) {
        return typeof x === 'number' && isFinite(x) && Math.floor(x) === x;
    }

    // ____________________________________________________
    // Check to see if number is a floating point.
    public isFloat(x) {
        return !!(x % 1);
    }

    // ____________________________________________________

    // Phone number regex function
    public phoneNumberRegex(fieldVal) {
        const re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        return re.test(fieldVal);
    }

    // Validate that 10 digit phone number is true
    public checkValidPhoneNumber(fieldVal) {
        return (this.phoneNumberRegex(fieldVal) != false) &&
            (fieldVal !== '') &&
            (fieldVal !== undefined) &&
            (fieldVal !== null);
    }

    // ____________________________________________________

    // Validate that field is a number integer
    public checkNumberValue(fieldVal) {
        const newValNum = Number(fieldVal);
        return (Number.isInteger(newValNum)) &&
            (newValNum !== undefined) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    public deepNumberValueCheck(fieldVal) {
        let newValNum;
        if ((this.isFloat(fieldVal)) || (this.isInteger(fieldVal))) {
            newValNum = fieldVal;
        } else {
            newValNum = Number(fieldVal);
        }

        return (newValNum !== 0) &&
            (newValNum !== undefined) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    public checkCCNumber(fieldVal) {
        const newValNum = Number(fieldVal);
        return (Number.isInteger(newValNum)) &&
            (newValNum !== undefined) &&
            (String(newValNum).length === 16) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    public checkCVCNumber(fieldVal) {
        const newValNum = Number(fieldVal);
        return (Number.isInteger(newValNum)) &&
            (newValNum !== undefined) &&
            (String(newValNum).length === 3) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    public checkCCMonth(fieldVal) {
        const newValNum = Number(fieldVal);
        return (Number.isInteger(newValNum)) &&
            (newValNum !== undefined) &&
            (String(newValNum).length === 2) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    public checkCCYear(fieldVal) {
        const newValNum = Number(fieldVal);
        return (Number.isInteger(newValNum)) &&
            (newValNum !== undefined) &&
            (String(newValNum).length === 4) &&
            (newValNum !== null);
    }

    // ____________________________________________________

    // Check Zip Code String Length
    public checkZipCode(fieldVal) {
        return ((/^\s*\d{5}\s*$/.test(fieldVal)) != false) &&
            (fieldVal !== '') &&
            (fieldVal !== undefined) &&
            (fieldVal !== null);
    }

    // ____________________________________________________

    // Check that Password fields are identical, match values and length
    public identicalPasswords(fieldVal, fieldValMatch) {
        const val1 = fieldVal;
        const val2 = fieldValMatch;

        function checkPasswordLength(valToCheck) {
            const checkMe = valToCheck;
            if (checkMe.length >= 8) {
                return true;
            } else {
                return false;
            }
        }

        if (val2 == val1) {
            if ((this.checkFieldNotEmpty(fieldVal)) &&
                (checkPasswordLength(fieldVal))) {
                return true;
            }
        } else {
            return false;
        }
    }

    // ____________________________________________________

}

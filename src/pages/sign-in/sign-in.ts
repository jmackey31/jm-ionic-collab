import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from '@angular/forms';

// Services
import {SiteHttpService} from '../../services/site-http.service';
import {SiteGlobalFunctionsService} from '../../services/site-global-functions.service';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-sign-in',
    templateUrl: 'sign-in.html',
})
export class SignInPage implements OnInit {
    public currentUserTypeState = 'user';

    // usersTabClass and vendorsStateActive
    public usersTabClass = 'login-tab-li li-active';
    public vendorTabClass = 'login-tab-li';

    public signInFormGroup: FormGroup;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public siteHTTPService: SiteHttpService,
        public siteGlobalFunctionsService: SiteGlobalFunctionsService) {
    }

    public ngOnInit() {
        // console.log(this.navParams);
        // console.log('this.navParams');

        // Initialize sign in form group
        this.signInFormGroup = new FormGroup({
            'username': new FormControl(null, Validators.required),
            'password': new FormControl(null, Validators.required)
        });

        // Load the user's username back into the form if the params data object has
        // a value of "sign_back_in"
        if (this.navParams.data === 'sign_back_in') {
            const toastArgsObject = {
                message: 'You were logged out for security reasons.',
                duration: 4000
            };
            this.siteGlobalFunctionsService.presentToastTemporary(toastArgsObject);

            this.signInFormGroup.patchValue({
                'username': localStorage.getItem('username'),
                'password': ''
            });

            // Check the current account type and and set the value
            const cachedAccountType = localStorage.getItem('account_type');
            if (cachedAccountType === 'user') {
                this.onClickUsersTab()
            } else if (cachedAccountType === 'vendor') {
                this.onClickVendorsTab()
            }
        } else {
            // console.log('User can log in as normal.');
        }
    }

    ionViewDidLoad() {
        // console.log('ionViewDidLoad SignInPage');
    }

    // public onNavigateToVendorProfilePage() {
    //     this.navCtrl.push(VendorProfilePage);
    // }

    public onClickUsersTab() {
        this.currentUserTypeState = 'user';
        this.usersTabClass = 'login-tab-li li-active';
        this.vendorTabClass = 'login-tab-li';

        // console.log('this.currentUserTypeState is listed below:');
        // console.log(this.currentUserTypeState);
    }

    public onClickVendorsTab() {
        this.currentUserTypeState = 'vendor';
        this.usersTabClass = 'login-tab-li';
        this.vendorTabClass = 'login-tab-li li-active';

        // console.log('this.currentUserTypeState is listed below:');
        // console.log(this.currentUserTypeState);
    }

    public navigateToForgotUsernamePasswordPage() {
        // this.navCtrl.push(ForgotUsernamePasswordPage);
    }

    public removeLocalStorageValues() {
        this.siteGlobalFunctionsService.removeUserLocalStorageValues();
    }

    public onSubmitSignIn() {
        const self = this;
        let authenticateURL;

        if (this.currentUserTypeState === 'user') {
            authenticateURL = 'user/authenticate';
        } else if (this.currentUserTypeState === 'vendor') {
            authenticateURL = 'vendor/authenticate';
        }

        const dataObject = {
            httpCallType: 'POST',
            url: authenticateURL,
            account_type: this.currentUserTypeState,
            username: this.signInFormGroup.value.username,
            password: this.signInFormGroup.value.password
        };
        // console.log(dataObject);

        this.siteGlobalFunctionsService.onActivatePleaseWaitBlock();

        this.siteHTTPService.callHTTPMethod(dataObject, function(data) {
            self.siteGlobalFunctionsService.onDeactivatePleaseWaitBlock();

            if (data.success === true) {
                // Clear out any old localStorage values
                self.removeLocalStorageValues();

                // Store new values in localStorage
                // self.siteGlobalFunctionsService.setUserLocalStorageValues(data);

                // Refresh the page to handle re-routing
                window.location.reload(true);

                // Send user to the My Account Page
                // self.navCtrl.push(MyAccountView);
            } else if (data.status === 403) {
                const toastArgsObject = {
                    message: data.message,
                    duration: 4000
                };
                self.siteGlobalFunctionsService.presentToastTemporary(toastArgsObject);
            } else if (data.success === false) {
                // alert('Awww man!  No go.');
                // console.log('No dice on the call.');

                const toastArgsObject = {
                    message: data.message,
                    duration: 4000
                };
                self.siteGlobalFunctionsService.presentToastTemporary(toastArgsObject);
            }
        });
    }

}

import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";

// Services
import {SiteHttpService} from '../../services/site-http.service';
import {SiteGlobalFunctionsService} from '../../services/site-global-functions.service';

import {SignInPage} from '../sign-in/sign-in';

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-sign-up',
    templateUrl: 'sign-up.html',
})
export class SignUpPage implements OnInit {
    public currentUserTypeState = 'user';
    public signUpFormGroup: FormGroup;

    public signupComplete = false;

    // usersTabClass and vendorsStateActive
    public usersTabClass = 'login-tab-li li-active';
    public vendorTabClass = 'login-tab-li';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public siteHTTPService: SiteHttpService,
        public siteGlobalFunctionsService: SiteGlobalFunctionsService) {
    }

    public ngOnInit() {
        this.signUpFormGroup = new FormGroup({
            'email': new FormControl(null, Validators.required),
            'username': new FormControl(null, Validators.required),
            'password': new FormControl(null, Validators.required)
        });
    }

    ionViewDidLoad() {
        // console.log('ionViewDidLoad SignUpPage');
    }

    public onClickUsersTab() {
        this.currentUserTypeState = 'user';
        this.usersTabClass = 'login-tab-li li-active';
        this.vendorTabClass = 'login-tab-li';

        // console.log('this.currentUserTypeState is listed below:');
        // console.log(this.currentUserTypeState);
    }

    public onClickVendorsTab() {
        this.currentUserTypeState = 'vendor';
        this.usersTabClass = 'login-tab-li';
        this.vendorTabClass = 'login-tab-li li-active';

        // console.log('this.currentUserTypeState is listed below:');
        // console.log(this.currentUserTypeState);
    }

    public navigateToSignInPage() {
        this.navCtrl.push(SignInPage);
    }

    public onSubmitSignUp() {
        const self = this;
        let userCreateString;

        if (this.currentUserTypeState === 'user') {
            userCreateString = 'user/create';
        } else if (this.currentUserTypeState === 'vendor') {
            userCreateString = 'vendor/create';
        }

        const dataObject = {
            httpCallType: 'POST',
            url: userCreateString,
            account_type: this.currentUserTypeState,
            email: this.signUpFormGroup.value.email,
            username: this.signUpFormGroup.value.username,
            password: this.signUpFormGroup.value.password
        };
        // console.log('----------------------------');
        // console.log(dataObject);

        this.siteGlobalFunctionsService.onActivatePleaseWaitBlock();

        this.siteHTTPService.callHTTPMethod(dataObject, function(data) {
            self.siteGlobalFunctionsService.onDeactivatePleaseWaitBlock();

            if (data.success === true) {
                self.signupComplete = true;
            } else if (data.status === 403) {
                const toastArgsObject = {
                    message: data.message,
                    duration: 4000
                };
                self.siteGlobalFunctionsService.presentToastTemporary(toastArgsObject);
            } else if (data.success === false) {
                const toastArgsObject = {
                    message: data.message,
                    duration: 4000
                };
                self.siteGlobalFunctionsService.presentToastTemporary(toastArgsObject);
            }
        });
    }

}

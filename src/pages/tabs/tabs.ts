import {Component} from '@angular/core';

import {HomePage} from '../home/home';
import {AboutUsPage} from '../about-us/about-us';
import {CategoriesPage} from '../categories/categories';
import {MyEventsPage} from '../my-events/my-events';

@Component({
    selector: 'tabs-block',
    templateUrl: 'tabs.html'
})
export class TabsPage {

    tab1Root = HomePage;
    tab2Root = AboutUsPage;
    tab3Root = CategoriesPage;
    tab4Root = MyEventsPage;

    constructor() {

    }
}

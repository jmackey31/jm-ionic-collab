import {Component, OnInit} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

import {BarcodeScanner} from '@ionic-native/barcode-scanner';

import {SiteHttpService} from '../../services/site-http.service';
import {SiteGlobalFunctionsService} from '../../services/site-global-functions.service';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {
    public showMyScannedResults: boolean = false;
    public myScannedResults: string;

    public location = {};

    constructor(
        public navCtrl: NavController,
        private barcode: BarcodeScanner,
        public navParams: NavParams,
        public siteHTTPService: SiteHttpService,
        public siteGlobalFunctionsService: SiteGlobalFunctionsService) {

    }

    public ngOnInit() {

    }

    async scanBarcode() {
        const RESULTS = await this.barcode.scan();
        const MY_STRING_OBJECT = JSON.stringify(RESULTS.text);
        // const SLICED_STRING = MY_STRING_OBJECT.slice(33,59);

        alert('Now for the string below: ');
        console.log(MY_STRING_OBJECT);
        alert(MY_STRING_OBJECT);
    }

}

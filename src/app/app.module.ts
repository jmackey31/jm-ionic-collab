import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {SortablejsModule} from 'angular-sortablejs';
// import {NgxStripeModule} from 'ngx-stripe';
import {Stripe} from '@ionic-native/stripe';

import {HomePage} from '../pages/home/home';
import {AboutUsPage} from '../pages/about-us/about-us';
import {CategoriesPage} from '../pages/categories/categories';
import {MyAccountPage} from '../pages/my-account/my-account';
import {MyEventsPage} from '../pages/my-events/my-events';
import {SearchPage} from '../pages/search/search';
import {SignInPage} from '../pages/sign-in/sign-in';
import {SignUpPage} from '../pages/sign-up/sign-up';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

// Services
import {SiteHttpService} from '../services/site-http.service';
import {SiteGlobalFunctionsService} from '../services/site-global-functions.service';
import {McValidationsService} from '../services/mc-validations.service';

// Other Modules
import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import {MomentModule} from 'angular2-moment';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        AboutUsPage,
        CategoriesPage,
        MyAccountPage,
        MyEventsPage,
        SearchPage,
        SignInPage,
        SignUpPage,
        TabsPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        MomentModule,
        // ChartModule,
        // InlineSVGModule,
        // InlineSVGModule.forRoot({ baseUrl: 'http://localhost:8100/' }),
        SortablejsModule.forRoot({ animation: 150 })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        AboutUsPage,
        CategoriesPage,
        MyAccountPage,
        MyEventsPage,
        SearchPage,
        SignUpPage,
        TabsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        BarcodeScanner,
        Stripe,
        // ChartModule,
        SiteHttpService,
        SiteGlobalFunctionsService,
        McValidationsService,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
